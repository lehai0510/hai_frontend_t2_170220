<?php

namespace Bss\Internship\Observer;

use Magento\Framework\Event\ObserverInterface;

class CustomPriceCategoryPage implements ObserverInterface
{
    /**
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $customPrice = 10;
        $items = $observer->getEvent()->getCollection();
        foreach ($items as $item) {
            $item->setPrice($customPrice);
        }
    }
}

<?php

namespace Bss\Internship\Observer;

use Magento\Framework\Event\ObserverInterface;

class CustomPriceAddToCart implements ObserverInterface
{
    /**
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        // Get the quote item
        $item = $observer->getQuoteItem();
        // Ensure we have the parent item, if it has one
        $item = ($item->getParentItem() ? $item->getParentItem() : $item);
        // Load the custom price
        $price = 10;
        // Set the custom price
        $item->setCustomPrice($price);
        $item->setOriginalCustomPrice($price);
        // Enable super mode on the product.
        $item->getProduct()->setIsSuperMode(true);

    }
}

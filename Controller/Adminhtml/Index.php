<?php

namespace Bss\Internship\Controller\Adminhtml;

use Magento\Framework\App\Action\Action;

class Index extends Action
{
    /**
     * @inheritDoc
     */
    public function execute()
    {
        // TODO: Implement execute() method.
    }

    /**
     * @return mixed
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magento_Customer::manage');
    }
}

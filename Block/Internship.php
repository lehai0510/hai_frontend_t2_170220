<?php

namespace Bss\Internship\Block;

use Bss\Internship\Model\InternshipFactory;
use Magento\Framework\View\Element\Template;
use Bss\Internship\Api\InternshipRepositoryInterface;

class Internship extends Template
{
    /**
     * @var InternshipFactory
     */
    protected $_internshipFactory;
    /**
     * @var InternshipRepositoryInterface
     */
    protected $_internshipRepository;

    /**
     * Internship constructor.
     * @param Template\Context $context
     * @param InternshipFactory $internshipFactory
     * @param InternshipRepositoryInterface $internshipRepository
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        InternshipFactory $internshipFactory,
        InternshipRepositoryInterface $internshipRepository,
        array $data = []
    ) {
        $this->_internshipRepository = $internshipRepository;
        $this->_internshipFactory = $internshipFactory;
        parent::__construct($context, $data);
    }

    /**
     * @return Template
     */
    public function _prepareLayout()
    {
        return $this->_internshipFactory->create()->load(3);
    }
}

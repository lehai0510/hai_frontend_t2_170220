<?php

namespace Bss\Internship\Model;

use Bss\Internship\Api\InternshipRepositoryInterface;
use Magento\Framework\Exception\NoSuchEntityException;

class InternshipRepository implements InternshipRepositoryInterface
{
    protected $resource;
    protected $internshipFactory;

    /**
     * InternshipRepository constructor.
     * @param ResourceModel\Internship $resource
     * @param InternshipFactory $blockFactory
     */
    public function __construct(
        \Bss\Internship\Model\ResourceModel\Internship $resource,
        InternshipFactory $blockFactory
    ) {
        $this->resource = $resource;
        $this->internshipFactory = $blockFactory;
    }

    /**
     * @param int $InternshipId
     * @return \Bss\Internship\Api\Data\InternshipInterface
     * @throws NoSuchEntityException
     */
    public function getById($InternshipId)
    {
        $block = $this->internshipFactory->create();
        $this->resource->load($block, $InternshipId);
        if (!$block->getId()) {
            throw new NoSuchEntityException(__('The CMS block with the "%1" ID doesn\'t exist.', $InternshipId));
        }
        return $block;
    }
}

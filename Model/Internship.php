<?php
namespace Bss\Internship\Model;

use Bss\Internship\Api\Data\InternshipInterface;

class Internship extends \Magento\Framework\Model\AbstractModel implements InternshipInterface
{
    const CACHE_TAG = 'bss_internship_internship';

    protected $_cacheTag = 'bss_internship_internship';

    protected $_eventPrefix = 'bss_internship_internship';

    /**
     * Construct
     */
    protected function _construct()
    {
        $this->_init('Bss\Internship\Model\ResourceModel\Internship');
    }

    /**
     * @return mixed
     */
    public function getInternshipName()
    {
        return $this->getData('name');
    }
}

<?php
namespace Bss\Internship\Model\ResourceModel;

class Internship extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Internship constructor.
     * @param \Magento\Framework\Model\ResourceModel\Db\Context $context
     */
    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context
    ) {
        parent::__construct($context);
    }

    /**
     * Internship constructor.
     */
    protected function _construct()
    {
        $this->_init('internship', 'id');
    }
}

<?php
namespace Bss\Internship\Model\ResourceModel\Internship;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'id';
    protected $_eventPrefix = 'bss_internship_internship_collection';
    protected $_eventObject = 'internship_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Bss\Internship\Model\Internship', 'Bss\Internship\Model\ResourceModel\Internship');
    }

}

<?php
namespace Bss\Internship\Api;

use Magento\Framework\Exception\LocalizedException;
use Bss\Internship\Api\Data\InternshipInterface;

interface InternshipRepositoryInterface
{
    /**
     * Retrieve block.
     *
     * @param int $InternshipId
     * @return InternshipInterface
     * @throws LocalizedException
     */
    public function getById($InternshipId);
}

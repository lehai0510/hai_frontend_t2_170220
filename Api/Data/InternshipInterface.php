<?php

namespace Bss\Internship\Api\Data;

interface InternshipInterface
{
    /**
     * @inheritDoc
     *
     * @return mixed
     */
    public function getInternshipName();
}
